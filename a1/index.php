<?php require_once('./code.php') ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S04 Activity</title>
</head>
<body>
    <h1>Building</h1>
    <p>The name of the building is <?= $building->getName(); ?></p><br>

    <p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors.</p><br>

    <p><?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p><br>

    <?php $building->setName("Caswynn Complex"); ?>

    <p>The name of the building has been changed to <?= $building->getName(); ?></p>
    
    <hr>

    <h1>Condominium</h1>
    <p>The name of the condominium is <?= $condominium->getName(); ?></p><br>

    <p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p><br>

    <p><?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p><br>

    <?php $condominium->setName("Enzo Tower"); ?>

    <p>The name of the condominium has been changed to <?= $condominium->getName(); ?></p>
</body>
</html>