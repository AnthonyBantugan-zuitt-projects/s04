<?php

// [SECTION] Access Modifiers
// Each property and method inside of a class can be given a certain access modifier

// public means that the property/method is accessible be reassigned/changed by anyone

// private means that direct access to an objects property is disabled and cannot be changed by anyone
// it also means that inheritance of its properties and methods is also disabled

// protected also disables direct access to an objects properties and methods, but inheritance is still allowed
// take note that the protected access modifier is ALSO inherited to the child class.

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
}

// [SECTION] Encapulation
// Using what are called getter methods and setter methods, we can implement encapulation of an objects data
// Getters and setters serve as an intermediary in accessing or objects properties or methods
//Getters and setters work for both private and protected properties
//  you do not always need a getter and setter for every property
class Condominium extends Building {
    public function getName(){
        return $this->name;
    }

    public function setName($name){
        return $this->name = $name;
    }
   
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City Philippines');